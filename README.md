Compiling
=================
Before compiling put in `src/main/resources/application.yml` datasource config.

`mvn package` will create war file.

Datasource config
=================
You can launch application on mysql, or hsqldb, or with installed in jndi data source.
By default starts hsqldb database. It can be changed in `src/main/resources/application.yml` part `spring.datasource`.
There is commented examples for every case.

Also you should change dialect in `spring.jpa.hibernate.dialect`

String `spring.jpa.hibernate.ddl_auto: create-drop` will make application on launch create (recreate) schema on startup.
If you already have schema and some data, you should comment out this string.

Running
=================

You can either launch with spring boot or deploy war on application server.

1. With spring boot you can launch application by `mvn spring-boot:run` command. 
It will launch embedded tomcat server on port 8080.
2. To deploy on tomcat put `backend.war` in webapp directory.

Application tested on tomcat version 7.0.61  