package ru.astafev.test.crossover_backend.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(exclude="orderLines")
public class OrderSales {

	@Id
	@Column(length = 32)
	@Size(max = 32)
	private String orderNumber;
	
	private BigDecimal totalPrice;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<OrderLine> orderLines;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
}
