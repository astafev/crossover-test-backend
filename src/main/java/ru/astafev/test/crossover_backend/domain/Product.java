package ru.astafev.test.crossover_backend.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Product {

	@Id
	private String code;
	
	private String description;
	
	private BigDecimal price;
	
	private Integer quantity;

}
