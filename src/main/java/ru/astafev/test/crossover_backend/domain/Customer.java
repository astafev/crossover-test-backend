package ru.astafev.test.crossover_backend.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
public class Customer {

	@Id
	@NotNull
	private String code;

	@Size(max = 256)
	private String name;

	@Size(max = 512)
	private String address;

	@Size(max = 50)
	private String phone1;

	@Size(max = 50)
	private String phone2;
	
	private BigDecimal creditLimit;
	
	private BigDecimal currentCredit;
}
