package ru.astafev.test.crossover_backend.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.astafev.test.crossover_backend.domain.Customer;

public interface CustomersRepository extends CrudRepository<Customer, String>{
}
