package ru.astafev.test.crossover_backend.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.astafev.test.crossover_backend.domain.Product;

import java.math.BigDecimal;

public interface ProductsRepository extends CrudRepository<Product, String> {
}
