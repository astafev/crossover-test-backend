package ru.astafev.test.crossover_backend.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.astafev.test.crossover_backend.domain.OrderSales;

public interface OrdersRepository extends CrudRepository<OrderSales, String> {
}
