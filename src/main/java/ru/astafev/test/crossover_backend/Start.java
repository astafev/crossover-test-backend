package ru.astafev.test.crossover_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EntityScan(basePackages = "ru.astafev.test.crossover_backend.domain")
@EnableJpaRepositories(basePackages = "ru.astafev.test.crossover_backend.repositories")
public class Start extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Start.class);
	}

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(Start.class);

        springApplication.setShowBanner(false);

        springApplication.run(args);
	}
}
