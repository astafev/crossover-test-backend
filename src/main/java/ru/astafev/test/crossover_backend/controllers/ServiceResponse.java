package ru.astafev.test.crossover_backend.controllers;

import lombok.Data;

import java.io.Serializable;

/**
 * Object for serialization
 */
@Data
public class ServiceResponse implements Serializable {

    static final long serialVersionUID = 1L;

    String error;
    boolean success;
    Object result;

    private ServiceResponse(){}

    public static ServiceResponse failure(Exception ex) {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.success = false;
        if (ex != null)
            serviceResponse.error = ex.getMessage();
        return serviceResponse;
    }

    public static ServiceResponse success() {
        return success(null);
    }

    public static ServiceResponse success(Object result) {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.success = true;
        serviceResponse.result = result;
        return serviceResponse;
    }

}
