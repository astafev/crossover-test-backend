package ru.astafev.test.crossover_backend.controllers;

import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ru.astafev.test.crossover_backend.domain.Customer;
import ru.astafev.test.crossover_backend.repositories.CustomersRepository;

@RestController
@RequestMapping(value="/customers")
@Slf4j
public class CustomersController {

	@Autowired
	CustomersRepository repository;
	
	@RequestMapping(method = {RequestMethod.POST})
	public ServiceResponse save(@RequestBody @Valid Customer customer) {
		log.info("saving {}", customer);
		repository.save(customer);
		return ServiceResponse.success();
	}
}
