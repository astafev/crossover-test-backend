package ru.astafev.test.crossover_backend.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.astafev.test.crossover_backend.domain.Customer;
import ru.astafev.test.crossover_backend.domain.OrderLine;
import ru.astafev.test.crossover_backend.domain.OrderSales;
import ru.astafev.test.crossover_backend.domain.Product;
import ru.astafev.test.crossover_backend.repositories.CustomersRepository;
import ru.astafev.test.crossover_backend.repositories.OrdersRepository;
import ru.astafev.test.crossover_backend.repositories.ProductsRepository;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.HashMap;

@RestController
@RequestMapping(value = "orders")
@Slf4j
public class OrdersController {

    @Autowired
    OrdersRepository repository;

    @Autowired
    CustomersRepository customersRepository;

    @Autowired
    ProductsRepository productsRepository;

    @RequestMapping(method = {RequestMethod.POST})
    public ServiceResponse save(@RequestBody @Valid OrderSales o) {
        validateOrder(o);
        log.info("saving {}", o);
        repository.save(o);
        return ServiceResponse.success();
    }

    void validateOrder(OrderSales o) {
        Customer customer = customersRepository.findOne(o.getCustomer().getCode());
        BigDecimal currentCredit = customer.getCurrentCredit() == null ? BigDecimal.ZERO : customer.getCurrentCredit();
        BigDecimal creditLimit = customer.getCreditLimit() == null ? BigDecimal.ZERO : customer.getCreditLimit();
        if (o.getTotalPrice().compareTo(creditLimit.subtract(currentCredit)) > 0) {
            throw new IllegalArgumentException("Order price exceeds credit limit");
        }
        customer.setCurrentCredit(currentCredit.subtract(o.getTotalPrice()));
        o.setCustomer(customer);
        HashMap<String, Product> products = new HashMap<>(o.getOrderLines().size());
        for (OrderLine ol : o.getOrderLines()) {
            String productCode = ol.getProduct().getCode();
            Product product = products.get(productCode);
            if (product == null) {
                product = productsRepository.findOne(productCode);
                products.put(product.getCode(), product);
            }
            if (product.getQuantity() < ol.getQuantity()) {
                throw new IllegalArgumentException("Not enough " + product.getDescription() + "(" + product.getCode() + ")");
            }
            product.setQuantity(product.getQuantity() - ol.getQuantity());
            ol.setOrder(o);
            ol.setProduct(product);
        }
    }
}
