package ru.astafev.test.crossover_backend.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;
import ru.astafev.test.crossover_backend.repositories.CustomersRepository;
import ru.astafev.test.crossover_backend.repositories.OrdersRepository;
import ru.astafev.test.crossover_backend.repositories.ProductsRepository;

@RestController
@Slf4j
@SuppressWarnings("unused")
public class CommonController {
    @Autowired
    CustomersRepository customersRepository;
    @Autowired
    ProductsRepository productsRepository;
    @Autowired
    OrdersRepository ordersRepository;

    protected static final String catalogLocation = "/{objectType:customers|products|orders}";
    protected static final String resourceLocation = catalogLocation + "/{code}";

    @RequestMapping(value = resourceLocation, method = RequestMethod.GET)
    public ServiceResponse requestByCode(@PathVariable String objectType, @PathVariable String code) {
        Object result = getRepository(objectType).findOne(code);
        log.info("requested {}", result);
        return ServiceResponse.success(result);
    }

    @RequestMapping(value = catalogLocation, method = RequestMethod.GET)
    public ServiceResponse list(@PathVariable String objectType) {
        Object result = getRepository(objectType).findAll();
        log.info("requested {}", result);
        return ServiceResponse.success(result);
    }

    @RequestMapping(value = resourceLocation, method = RequestMethod.DELETE)
    public ServiceResponse deleteByCode(@PathVariable String code, @PathVariable String objectType) {
        log.info("deleting {} by code {}", objectType, code);
        getRepository(objectType).delete(code);
        return ServiceResponse.success();
    }

    private CrudRepository<?, String> getRepository(String objectType) {
        switch (objectType) {
            case "customers":
                return this.customersRepository;
            case "products":
                return this.productsRepository;
            case "orders":
                return this.ordersRepository;
            default:
                throw new IllegalArgumentException("unknown object type");
        }
    }
}
