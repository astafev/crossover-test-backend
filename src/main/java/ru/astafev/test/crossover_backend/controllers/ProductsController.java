package ru.astafev.test.crossover_backend.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.astafev.test.crossover_backend.domain.Product;
import ru.astafev.test.crossover_backend.repositories.ProductsRepository;

import javax.validation.Valid;

@RestController
@RequestMapping("/products")
@Slf4j
public class ProductsController {
    @Autowired
    ProductsRepository repository;

    /**
     * @return price of product if it's found, null otherwise
     * */
    /*@RequestMapping("/{code}/price")
    public ServiceResponse getProductPrice(@PathVariable("code") String code) {
        return ServiceResponse.success(repository.getPriceByCode(code));
    }*/

    @RequestMapping(method = {RequestMethod.POST})
    public ServiceResponse save(@RequestBody @Valid Product p) {
        log.info("saving {}", p);
        repository.save(p);
        return ServiceResponse.success();
    }

}
