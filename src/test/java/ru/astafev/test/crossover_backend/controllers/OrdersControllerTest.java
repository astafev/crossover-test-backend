package ru.astafev.test.crossover_backend.controllers;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import ru.astafev.test.crossover_backend.domain.Customer;
import ru.astafev.test.crossover_backend.domain.OrderLine;
import ru.astafev.test.crossover_backend.domain.OrderSales;
import ru.astafev.test.crossover_backend.domain.Product;
import ru.astafev.test.crossover_backend.repositories.CustomersRepository;
import ru.astafev.test.crossover_backend.repositories.OrdersRepository;
import ru.astafev.test.crossover_backend.repositories.ProductsRepository;

import java.math.BigDecimal;
import java.util.ArrayList;

public class OrdersControllerTest {
    OrdersController ordersController;
    Customer testCustomer;

    {
        testCustomer = new Customer();
        testCustomer.setName("Test Customer's Name");
        testCustomer.setCode("test_customer");
        testCustomer.setAddress("Test Address");
        testCustomer.setCreditLimit(new BigDecimal(12345));
        testCustomer.setCurrentCredit(BigDecimal.ZERO);
        testCustomer.setPhone1("phone1");
    }

    Product testProduct;

    {
        testProduct = new Product();
        testProduct.setCode("test_product");
        testProduct.setDescription("Test Product Description");
        testProduct.setPrice(new BigDecimal(123));
        testProduct.setQuantity(123);
    }

    OrderSales testOrder;

    {
        testOrder = new OrderSales();
        testOrder.setCustomer(testCustomer);
        testOrder.setOrderLines(new ArrayList<OrderLine>(1));

        OrderLine e = new OrderLine();
        e.setOrder(testOrder);
        e.setProduct(testProduct);
        e.setQuantity(1);
        e.setTotalPrice(new BigDecimal(123));
        e.setUnitPrice(new BigDecimal(123));
        testOrder.getOrderLines().add(e);
        testOrder.setTotalPrice(new BigDecimal(123));
    }

    @Before
    public void createOrdersControllerWithMockedRepos() {
        ordersController = new OrdersController();
        ordersController.customersRepository = mock(CustomersRepository.class);

        when(ordersController.customersRepository.findOne("test_customer")).thenReturn(testCustomer);

        ordersController.productsRepository = mock(ProductsRepository.class);
        when(ordersController.productsRepository.findOne("test_product")).thenReturn(testProduct);

        ordersController.repository = mock(OrdersRepository.class);

    }

    @Test(expected = Exception.class)
    public void testErrorValidateOnCreditLimit() {
        try {
            testCustomer.setCurrentCredit(testCustomer.getCreditLimit());
            ordersController.validateOrder(testOrder);
        } finally {
            testCustomer.setCurrentCredit(new BigDecimal(12345));
        }

    }

    @Test(expected = Exception.class)
    public void testErrorValidateOnZeroCredit() {
        try {
            testCustomer.setCreditLimit(BigDecimal.ZERO);
            ordersController.validateOrder(testOrder);
        } finally {
            testCustomer.setCreditLimit(new BigDecimal(12345));
        }
    }

    @Test(expected = Exception.class)
    public void testErrorValidateOnQuantityExceed() {
        try {
            testProduct.setQuantity(testOrder.getOrderLines().get(0).getQuantity() - 1);
            ordersController.validateOrder(testOrder);
        } finally {
            testProduct.setQuantity(123);
        }
    }

    @Test
    public void testCorrectnessOnTestData() {
        ordersController.validateOrder(testOrder);
    }


}
